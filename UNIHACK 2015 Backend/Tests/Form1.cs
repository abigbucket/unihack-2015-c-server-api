﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DBLib;
using Logging;
using System.Security.Cryptography;

namespace Tests
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            /*User u = new User();
            u.hidingPoints = 4;
            u.name = "JoshC";
            u.password = "1234";
            u.photoURL = "";
            u.salt = 0xFFDDCC08;
            u.seekingPoints = 2;
            u.userFriends = ":'(";
            u.username = "JoshC";

            MySql.Data.MySqlClient.MySqlConnection conn = new MySql.Data.MySqlClient.MySqlConnection();

            User g = UserDB.insertUser(u, null);
            Logging.LogHelper.Log(g.userID.ToString());*/

            /*User u = UserFunctions.login(tb1.Text, tb2.Text);

            Logging.LogHelper.Log(u.userID.ToString());*/

            /*Game g = new Game();
            g.boundary1Lat = "40.7486";
            g.boundary1Long = "-73.9864";
            g.boundary2Lat = "40.7486";
            g.boundary2Long = "-73.9864";
            g.finished = false;
            g.ownerID = 1;
            g.timeLength = new TimeSpan(4, 37, 23);
            g.timeStart = new DateTime(2015, 08, 15, 22, 0, 0);

            g = GameDB.insertGame(g, null);
            Logging.LogHelper.Log(g.gameID.ToString());*/

            /*Participant p = new Participant();
            p.userID = 1;
            p.gameID = 1;
            p.finderID = null;
            p.finished = false;
            p.foundList = "";
            p.hidePointsThisGame = 0;
            p.inactive = false;
            p.inactiveCount = 0;
            p.role = 1;     // NEED TO FIX THIS
            p.seekPointsThisGame = 0;

            p = ParticipantDB.insertParticipant(p, null);
            Logging.LogHelper.Log(p.participantID.ToString());*/

            ParticipantDB.countPlayerGroups(1, null);

            ParticipantFunctions.decideRoleOnAdd(1, null);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //LogHelper.Log("HI!");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            

            byte[] salt = { 0, 0, 0, 0, 0, 0, 0, 0 };

            Rfc2898DeriveBytes rfc = new Rfc2898DeriveBytes(tb1.Text, salt);

            byte[] hash = rfc.GetBytes(8);
            long hashu = BitConverter.ToInt64(hash, 0);
            //long hashl = BitConverter.ToInt64(hash, 8);

            tb2.Text = hashu.ToString(); //+ hashl.ToString();
        }
    }
}
