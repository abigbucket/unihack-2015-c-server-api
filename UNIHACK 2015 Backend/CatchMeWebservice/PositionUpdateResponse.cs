﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CatchMeWebservice
{
    public class PositionUpdateResponse
    {
        public short role;
        public bool inactive;
        public bool finished;
        public List<Tuple3<int, double, String>> targetInfo; //userID, distance, MAC Address
        public List<Int32> playerCounts; //player counts
        public int seekerPoints;
        public int hiderPoints;
    }
}