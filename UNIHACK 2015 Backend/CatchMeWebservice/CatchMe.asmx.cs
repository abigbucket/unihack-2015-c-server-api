﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DBLib;
using Logging;
using MySql.Data.MySqlClient;

namespace CatchMeWebservice
{
    /// <summary>
    /// Summary description for CatchMe
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class CatchMe : System.Web.Services.WebService
    {
        const double EARTHRADIUS = 6371000; //mean radius in m
        
        [WebMethod]
        public bool LoginCheck(string username, string password)
        {
            User u = DBLib.UserFunctions.login(username, password, null);

            if (u == null)
                return false;

            return true;
        }

        /*[WebMethod]
        public ICollection<Game> ListGamesNearby(string username, string password)
        {
            User u = DBLib.UserFunctions.login(username, password);

            if (u == null)
                return false;

            return true;
        }*/

        [WebMethod]
        public bool JoinGame(string username, string password, int gameID)
        {
            MySqlConnection conn = BaseDB.connect();

            User u = DBLib.UserFunctions.login(username, password, conn);

            if (u == null)
                return false;

            //Should check here if the game exists, is it in the future/currently running? Also don't let people join a game about to end.

            //Should check here if we have already joined
            //search by game + uID and reject if we find an existing entry

            Participant p = new Participant();
            p.userID = u.userID;
            p.gameID = gameID;
            p.finderID = null;
            p.finished = false;
            p.foundList = "";
            p.hidePointsThisGame = 0;
            p.inactive = false;
            p.inactiveCount = 0;
            p.role = ParticipantFunctions.decideRoleOnAdd(gameID, conn);
            p.seekPointsThisGame = 0;

            ParticipantDB.insertParticipant(p, conn);

            return true;
        }

        [WebMethod]
        public PositionUpdateResponse UpdatePosition(string username, string password, int gameID, int participantID, string lat, string lon)
        {
            MySqlConnection conn = BaseDB.connect();

            User u = DBLib.UserFunctions.login(username, password, conn);

            if (u == null)
                return null;
            
            //Application[] variables are persistent between calls. This allows the system to avoid querying large amounts of position data from the database.
            if (Application["LocationDictionary"] == null)
            {
                Application["LocationDictionary"] = new Dictionary<int, Tuple<double, double>>();//userID, lat, long
            }
            if (Application["MACList"] == null)
            {
                Application["MACList"] = new Dictionary<int, String>(); //userID, MAC Address
            }


            double dlat = -1;
            double dlon = -1;
            bool fail = Double.TryParse(lat, out dlat);
            fail = fail & Double.TryParse(lon, out dlon);

            if (!fail)
                return null;

            Participant part = ParticipantDB.getParticipant(participantID, conn);

            if (part.gameID != gameID || part.userID != u.userID) //inconsistent data sent
                return null;

            PositionUpdateResponse response = new PositionUpdateResponse();
            response.hiderPoints = part.hidePointsThisGame;
            response.seekerPoints = part.seekPointsThisGame;
            response.finished = part.finished;
            response.inactive = part.inactive;
            response.playerCounts = ParticipantDB.countPlayerGroups(part.gameID, conn);
            response.role = part.role;
            
            //Log The Lat/Long entry
            ParticipantLocation pLoc = new ParticipantLocation();
            pLoc.gameID = part.gameID;
            pLoc.latitude = lat;
            pLoc.longditude = lon;
            pLoc.participantID = part.participantID;
            pLoc.userID = u.userID;
            pLoc.logDate = DateTime.Now;
            
            ParticipantLocationDB.insertParticipantLocation(pLoc, conn);

            
            ((Dictionary<int, Tuple<double, double>>)Application["LocationDictionary"])[u.userID] = new Tuple<double, double>(dlat, dlon);
            List<Tuple3<int, double, String>> targetInfo = calculateDistance(((Dictionary<int, Tuple<double, double>>)Application["LocationDictionary"]), new Tuple<int, double, double>(u.userID, dlat, dlon));



            //Check whether it is in bounds
            Game game = GameDB.getGame(part.gameID, conn);
            double glat1 = -1;
            double glon1 = -1;
            double glat2 = -1;
            double glon2 = -1;

            //stuff it, leave it for later.
            

            //if still in, increment the points!
            if (part.role == 0 && part.inactive == false && part.finished == false)
            {
                part.hidePointsThisGame += 1;
                response.hiderPoints = part.hidePointsThisGame;
                response.seekerPoints = part.seekPointsThisGame;
                ParticipantDB.updateParticipant(part, conn); //writeback
            }




            for (int i = 0; i < targetInfo.Count; i++)
            {
                String MAC = null;
                if (((Dictionary<int, String>)Application["MACList"]).TryGetValue(targetInfo[i].Item1, out MAC))
                {
                    targetInfo[i]= new Tuple<int,double,string>(targetInfo[i].Item1, targetInfo[i].Item2, MAC);
                }
                else
                {
                    User utemp = UserDB.getUserByID(targetInfo[i].Item1, conn);
                    targetInfo[i] = new Tuple<int, double, string>(targetInfo[i].Item1, targetInfo[i].Item2, "FFFFFFFF00000000"); //insert User MAC Address
                }        
            }

            response.targetInfo = targetInfo;


            return response;
        }

        /*[WebMethod]
        public double dist()
        {
            Dictionary<int, Tuple<double, double>> di = new Dictionary<int, Tuple<double, double>>();
            di.Add(0, new Tuple<double, double>(20.0000, 25));
            Tuple<int, double, double> tu = new Tuple<int,double,double>(1, 20.0007 ,25);

            List<double> li = calculateDistance(di, tu);

            return li.First<double>();
        }*/


        private List<Tuple3<int, double, String>> calculateDistance(Dictionary<int, Tuple<double, double>> otherLocations, Tuple<int, double, double> point)
        {
            List<Tuple3<int, double, String>> dists = new List<Tuple3<int, double, String>>();

            foreach (KeyValuePair<int, Tuple<double, double>> kv in otherLocations)
            {
                if (kv.Key != point.Item1)
                {
                    double delLat = kv.Value.Item1 - point.Item2;

                    if (delLat > -0.0007 && delLat < 0.0007) //are they within 77m?
                    {
                        double lat1 = kv.Value.Item1 * 2 * Math.PI / 360;
                        double lon1 = kv.Value.Item2 * 2 * Math.PI /360;
                        double lat2 = point.Item2 * 2 * Math.PI / 360;
                        double lon2 = point.Item3 * 2 * Math.PI / 360;

                        delLat = delLat * 2 * Math.PI / 360;
                        double delLong = lon2 - lon1;

                        double a = (Math.Sin(delLat / 2)) * Math.Sin(delLat / 2) + Math.Cos(lat1) * Math.Cos(lat2) * Math.Sin(delLong / 2) * Math.Sin(delLong / 2);
                        double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
                        double D = EARTHRADIUS * c;

                        if (D < 80)     //Cut-off again at 80m.
                            dists.Add(new Tuple<int, double, String>(kv.Key, D, null));
                    }

                }
            }

            return dists;
        }


        [WebMethod]
        public void LeaveGame()
        {

        }

        [WebMethod]
        public void GetPhotos()
        {

        }

        [WebMethod]
        public void LodgeFound()
        {

        }

    }

    //Kludgey hack to get around the fact that the XML Serializer won't touch the in-built Tuples...
    public class Tuple2<T1, T2>
    {
        Tuple2() { }

        public T1 Item1 { get; set; }
        public T2 Item2 { get; set; }

        public static implicit operator Tuple2<T1, T2>(Tuple<T1, T2> t)
        {
            return new Tuple2<T1,T2>() {
                Item1 = t.Item1,
                Item2 = t.Item2
                };
        }

        public static implicit operator Tuple<T1, T2>(Tuple2<T1, T2> t)
        {
            return Tuple.Create(t.Item1, t.Item2);
        }

    }

    //Kludgey hack to get around the fact that the XML Serializer won't touch the in-built Tuples...
    public class Tuple3<T1, T2, T3>
    {
        Tuple3() { }

        public T1 Item1 { get; set; }
        public T2 Item2 { get; set; }
        public T3 Item3 { get; set; }

        public static implicit operator Tuple3<T1, T2, T3>(Tuple<T1, T2, T3> t)
        {
            return new Tuple3<T1, T2, T3>()
            {
                Item1 = t.Item1,
                Item2 = t.Item2,
                Item3 = t.Item3
            };
        }

        public static implicit operator Tuple<T1, T2, T3>(Tuple3<T1, T2, T3> t)
        {
            return Tuple.Create(t.Item1, t.Item2, t.Item3);
        }

    }
}
