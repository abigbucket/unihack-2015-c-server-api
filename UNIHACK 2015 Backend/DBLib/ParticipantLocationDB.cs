﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace DBLib
{
    public class ParticipantLocationDB
    {
        private static string SELECT_STRING = "SELECT * FROM participantlocation";
        private static string INSERT_STRING = @"INSERT INTO participantlocation(userID, gameID, participantID, logDate, 
                                        lat, lon) VALUES(@userID, @gameID, @participantID, @logDate, @lat, @long)";

        public static ParticipantLocation getParticipantLocationInternal(int participantLocationID, MySqlConnection conn, String specifier)
        {
            if (conn == null)
                conn = BaseDB.connect();

            String txt = SELECT_STRING + " WHERE participantLocationID=@PLID";

            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = txt;
            cmd.Parameters.AddWithValue("@PLID", participantLocationID);

            MySqlDataReader dr = cmd.ExecuteReader();
            return produceParticipantLocation(dr);
        }

        public static ParticipantLocation getMostRecentPL(int participantID, MySqlConnection conn, String specifier)
        {
            if (conn == null)
                conn = BaseDB.connect();

            String txt = SELECT_STRING + " WHERE participantID=@participantID ORDER BY logDate desc";

            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = txt;
            cmd.Parameters.AddWithValue("@participantID", participantID);

            MySqlDataReader dr = cmd.ExecuteReader();
            return produceParticipantLocation(dr);
        }



        public static ParticipantLocation insertParticipantLocation(ParticipantLocation p, MySqlConnection conn)
        {
            if (conn == null)
                conn = BaseDB.connect();

            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = INSERT_STRING;
            cmd = toParameters(p, cmd);

            int affectedRows = cmd.ExecuteNonQuery();
            if (affectedRows > 0)
            {
                p.participantLocationID = (int)cmd.LastInsertedId;
                return p;
            }

            return null;  //produceUser(dr);
        }

        private static MySqlCommand toParameters(ParticipantLocation pl, MySqlCommand msc)
        {
            msc.Parameters.AddWithValue("@gameID", pl.gameID);
            msc.Parameters.AddWithValue("@userID", pl.userID);
            msc.Parameters.AddWithValue("@participantID", pl.participantID);
            msc.Parameters.AddWithValue("@logDate", pl.logDate);
            msc.Parameters.AddWithValue("@lat", pl.latitude);
            msc.Parameters.AddWithValue("@long", pl.longditude);

            return msc;
        }

        private static ParticipantLocation produceParticipantLocation(MySqlDataReader dr)
        {
            if (dr.HasRows)
            {
                dr.Read();
                ParticipantLocation pl = new ParticipantLocation();

                pl.participantLocationID = dr.GetInt32("participantLocationID");
                pl.gameID = dr.GetInt32("gameID");
                pl.userID = dr.GetInt32("userID");
                pl.participantID = dr.GetInt32("participantID");
                pl.logDate = dr.GetDateTime("logDate");
                pl.latitude = dr.GetString("lat");
                pl.longditude = dr.GetString("lon");

                return pl;
            }
            else
                return null;
        }

    }
}
