﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

//Entity Framework/Data access layer
namespace DBLib
{
    public class GameDB
    {
        private static string SELECT_STRING = "SELECT * FROM game";
        private static string INSERT_STRING = @"INSERT INTO game(ownerID, boundary1Lat, boundary1Long, boundary2Lat, 
                                        boundary2Long, timeLength, timeStart, finished) VALUES(@ownerID, @boundary1Lat, @boundary1Long, 
                                        @boundary2Lat, @boundary2Long, @timeLength, @timeStart, @finished)";

        public static Game getGame(int gameID, MySqlConnection conn)
        {
            MySqlDataReader dr = null;

            try
            {
                if (conn == null)
                    conn = BaseDB.connect();

                String txt = SELECT_STRING + " WHERE gameID=@gameID";

                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = txt;
                cmd.Parameters.AddWithValue("@gameID", gameID);

                dr = cmd.ExecuteReader();
                return produceGame(dr);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
            }
        }




        public static Game insertGame(Game g, MySqlConnection conn)
        {
            if (conn == null)
                conn = BaseDB.connect();

            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = INSERT_STRING;
            cmd = toParameters(g, cmd);

            int rows = cmd.ExecuteNonQuery();

            if (rows > 0)
            {
                g.gameID = (int)cmd.LastInsertedId;
                return g;
            }

            return null;//produceUser(dr);
        }


        private static MySqlCommand toParameters(Game g, MySqlCommand msc)
        {
            msc.Parameters.AddWithValue("@ownerID", g.ownerID);
            msc.Parameters.AddWithValue("@boundary1Lat", g.boundary1Lat);
            msc.Parameters.AddWithValue("@boundary1Long", g.boundary1Long);
            msc.Parameters.AddWithValue("@boundary2Lat", g.boundary2Lat);
            msc.Parameters.AddWithValue("@boundary2Long", g.boundary2Long);
            msc.Parameters.AddWithValue("@timeLength", g.timeLength);
            msc.Parameters.AddWithValue("@timeStart", g.timeStart);
            msc.Parameters.AddWithValue("@finished", g.finished);

            return msc;
        }



        private static Game produceGame(MySqlDataReader dr)
        {
            if (dr.HasRows)
            {
                dr.Read();
                Game g = new Game();
                g.gameID = dr.GetInt32("gameID");
                g.ownerID = dr.GetInt32("ownerID");
                g.boundary1Lat = dr.GetString("boundary1Lat");
                g.boundary1Long = dr.GetString("boundary1Long");
                g.boundary2Lat = dr.GetString("boundary2Lat");
                g.boundary2Long = dr.GetString("boundary2Long");
                g.timeLength = dr.GetTimeSpan("timeLength");
                g.timeStart = dr.GetDateTime("timeStart");
                g.finished = dr.GetBoolean("finished");

                return g;
            }
            else
                return null;
        }

    }
}
