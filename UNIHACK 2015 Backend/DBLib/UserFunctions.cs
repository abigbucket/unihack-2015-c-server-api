﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace DBLib
{
    public class UserFunctions
    {
        public static User login(string username, string password, MySql.Data.MySqlClient.MySqlConnection conn)
        {
            User u = UserDB.getUser(username, conn);

            if (u != null)
            {
                long trueHash = Int64.Parse(u.password);

                long val = hash(password, BitConverter.GetBytes(u.salt));
                if (val == trueHash)
                    return u;
                else
                    return null;
            }
            return null;
        }

        public static byte[] generateSalt()
        {
            byte[] salt = new byte[8];
            Random rnd = new Random();
            rnd.NextBytes(salt);

            return salt;
        }

        public static long hash(string text, byte[] salt)
        {
            Rfc2898DeriveBytes rfc = new Rfc2898DeriveBytes(text, salt);

            byte[] hash = rfc.GetBytes(8);
            long hashu = BitConverter.ToInt64(hash, 0);

            return hashu;
        }



    }
}
