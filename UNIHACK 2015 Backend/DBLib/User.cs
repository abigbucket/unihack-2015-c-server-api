﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBLib
{

    public class User
    {
        public int userID;
        public string name;
        public string photoURL;
        public string userFriends;
        public Int64 hidingPoints;
        public Int64 seekingPoints;
        public string username;
        public string password;
        public Int64 salt;

    }
}
