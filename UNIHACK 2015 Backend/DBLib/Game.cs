﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBLib
{
    public class Game
    {
        public int gameID;
        public int ownerID;
        public String boundary1Lat;
        public String boundary1Long;
        public String boundary2Lat;
        public String boundary2Long;
        public TimeSpan timeLength;
        public DateTime timeStart;
        public bool finished;
        
    }
}
