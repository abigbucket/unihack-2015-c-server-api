﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace DBLib
{
    public class ParticipantDB
    {
        private static string SELECT_STRING = "SELECT * FROM participant";
        private static string INSERT_STRING = @"INSERT INTO participant(gameID, userID, role, hidePointsThisGame, 
                                        seekPointsThisGame, inactive, inactiveCount, finished, finderID, foundList) VALUES(@gameID, @userID, @role,
                                        @hidePointsThisGame, @seekPointsThisGame, @inactive, @inactiveCount, @finished, @finderID, @foundList)";

        private static string UPDATE_STRING = @"UPDATE participant SET gameID=@gameID, userID=@userID, role=@role, hidePointsThisGame=@hidePointsThisGame, 
                                                seekPointsThisGame=@seekPointsThisGame, inactive=@inactive, inactiveCount=@inactiveCount, finished=@finished,
                                                finderID=@finderID, foundList=@foundList WHERE participantID=@participantID";

        private static string COUNT_STRING = @"SELECT role, COUNT(*) FROM participant WHERE gameID=@gameID and finished = 0 and inactive = 0 GROUP BY role";

        public static Participant getParticipant(int participantID, MySqlConnection conn)
        {
            MySqlDataReader dr = null;

            try
            {
                if (conn == null)
                    conn = BaseDB.connect();

                String txt = SELECT_STRING + " WHERE participantID=@participantID";

                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = txt;
                cmd.Parameters.AddWithValue("@participantID", participantID);

                dr = cmd.ExecuteReader();
                return produceParticipant(dr);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
            }
        }

        public static Participant insertParticipant(Participant p, MySqlConnection conn)
        {
            if (conn == null)
                conn = BaseDB.connect();

            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = INSERT_STRING;
            cmd = toParameters(p, cmd);

            int affectedRows = cmd.ExecuteNonQuery();
            if (affectedRows > 0)
            {
                p.participantID = (int)cmd.LastInsertedId;
                return p;
            }

            return null;  //produceUser(dr);

        }

        public static List<Int32> countPlayerGroups(int gameID, MySqlConnection conn)
        {
            MySqlDataReader dr = null;

            try
            {
                List<Int32> li = new List<int>();

                for (int i = 0; i < 3; i++)
                    li.Add(0);  //set up items

                if (conn == null)
                    conn = BaseDB.connect();

                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = COUNT_STRING;
                cmd.Parameters.AddWithValue("@gameID", gameID);

                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        int role = dr.GetInt16(0);
                        int count = dr.GetInt32(1);

                        if (role < 3)
                        {
                            li[role] = count;
                        }
                    }

                }

                return li;

            }
            finally
            {
                if (dr != null)
                    dr.Close();
            }
        }




        //Updates a single participant by ID
        public static Participant updateParticipant(Participant p, MySqlConnection conn)
        {
            if (conn == null)
                conn = BaseDB.connect();

            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = UPDATE_STRING;
            cmd = toParameters(p, cmd);
            cmd.Parameters.AddWithValue("@participantID", p.participantID);

            int affectedRows = cmd.ExecuteNonQuery();
            if (affectedRows > 0)
            {
                p.participantID = (int)cmd.LastInsertedId;
                return p;
            }

            return null;  //produceUser(dr);
        }

        private static MySqlCommand toParameters(Participant p, MySqlCommand msc)
        {
            msc.Parameters.AddWithValue("@gameID", p.gameID);
            msc.Parameters.AddWithValue("@userID", p.userID);
            msc.Parameters.AddWithValue("@role", p.role);
            msc.Parameters.AddWithValue("@hidePointsThisGame", p.hidePointsThisGame);
            msc.Parameters.AddWithValue("@seekPointsThisGame", p.seekPointsThisGame);
            msc.Parameters.AddWithValue("@inactive", p.inactive);
            msc.Parameters.AddWithValue("@inactiveCount", p.inactiveCount);
            msc.Parameters.AddWithValue("@finished", p.finished);
            msc.Parameters.AddWithValue("@finderID", p.finderID);
            msc.Parameters.AddWithValue("@foundList", p.foundList);
            
            return msc;
        }

        private static Participant produceParticipant(MySqlDataReader dr)
        {
            if (dr.HasRows)
            {
                dr.Read();
                Participant p = new Participant();

                p.participantID = dr.GetInt32("participantID");
                p.gameID = dr.GetInt32("gameID");
                p.userID = dr.GetInt32("userID");
                p.role = dr.GetInt16("role");
                p.hidePointsThisGame = dr.GetInt32("hidePointsThisGame");
                p.seekPointsThisGame = dr.GetInt32("seekPointsThisGame");
                p.inactive = dr.GetBoolean("inactive");
                p.inactiveCount = dr.GetInt16("inactiveCount");
                p.finished = dr.GetBoolean("finished");
                p.finderID = dr.GetInt32("finderID");
                p.foundList = dr.GetString("foundList");

                return p;
            }
            else
                return null;
        }


    }
}
