﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data;
using MySql.Data.MySqlClient;


namespace DBLib
{
    public class UserDB
    {
        private static string SELECT_STRING = "SELECT * FROM user";
        private static string INSERT_STRING = @"INSERT INTO user(photoURL, userFriends, hidingPoints, seekingPoints, 
                                        username, password, salt) VALUES(@photoURL, @userFriends, @hidingPoints, 
                                        @seekingPoints, @username, @password, @salt)";

        public static User getUserByID(int userID, MySqlConnection conn)
        {
            MySqlDataReader dr = null;

            try
            {
                if (conn == null)
                    conn = BaseDB.connect();

                String txt = SELECT_STRING + " WHERE userID=@uID";

                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = txt;
                cmd.Parameters.AddWithValue("@uID", userID);

                dr = cmd.ExecuteReader();
                return produceUser(dr);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
            }
        }



        public static User getUser(string username, MySqlConnection conn)
        {
            MySqlDataReader dr = null;

            try
            {
                if (conn == null)
                    conn = BaseDB.connect();

                String txt = SELECT_STRING + " WHERE username=@uname";

                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = txt;
                cmd.Parameters.AddWithValue("@uname", username);

                dr = cmd.ExecuteReader();
                return produceUser(dr);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
            }
        }

        public static User insertUser(User u, MySqlConnection conn)
        {
            if (conn == null)
                conn = BaseDB.connect();

            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = INSERT_STRING;
            cmd = toParameters(u, cmd);

            int affectedRows = cmd.ExecuteNonQuery();
            if (affectedRows > 0)
            {
                u.userID = (int)cmd.LastInsertedId;
                return u;
            }

            return null;  //produceUser(dr);
        }

        private static MySqlCommand toParameters(User u, MySqlCommand msc)
        {
            msc.Parameters.AddWithValue("@photoURL", u.photoURL);
            msc.Parameters.AddWithValue("@userFriends", u.userFriends);
            msc.Parameters.AddWithValue("@hidingPoints", u.hidingPoints);
            msc.Parameters.AddWithValue("@seekingPoints", u.seekingPoints);
            msc.Parameters.AddWithValue("@username", u.username);
            msc.Parameters.AddWithValue("@password", u.password);
            msc.Parameters.AddWithValue("@salt", u.salt);

            return msc;
        }

        private static User produceUser(MySqlDataReader dr)
        {
            if (dr.HasRows)
            {
                dr.Read();
                User u = new User();
                u.userID = dr.GetInt32("userID");
                u.username = dr.GetString("username");
                u.photoURL = dr.GetString("photoURL");
                u.userFriends = dr.GetString("userFriends");
                u.hidingPoints = dr.GetInt32("hidingPoints");
                u.seekingPoints = dr.GetInt32("seekingPoints");
                u.password = dr.GetString("password");
                u.salt = dr.GetInt64("salt");

                return u;
            }
            else
                return null;
        }


    }
}
