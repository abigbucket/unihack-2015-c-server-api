﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBLib
{
    public class ParticipantLocation
    {
        public int participantLocationID;
        public int userID;
        public int gameID;
        public int participantID;
        public DateTime logDate;
        public String latitude;
        public String longditude;
    }
}
