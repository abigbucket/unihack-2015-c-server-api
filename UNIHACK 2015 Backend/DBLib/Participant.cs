﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBLib
{
    public class Participant
    {
        public int participantID;
        public int userID;
        public int gameID;
        public short role;
        public int hidePointsThisGame;
        public int seekPointsThisGame;
        public bool inactive;
        public short inactiveCount;
        public bool finished;
        public int? finderID;
        public String foundList;

    }
}
