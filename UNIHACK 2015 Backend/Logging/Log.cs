﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Logging
{
    public class LogHelper
    {
        private static String logFile = "\\Log" + DateTime.Now.ToString("yy-MM-dd") + ".txt";


        public static void Log(String s)
        {
            //string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            string path = AppDomain.CurrentDomain.BaseDirectory;
            Console.WriteLine(path + logFile);
            StreamWriter sw = new StreamWriter(path + logFile, true);
            try
            {
                sw.WriteLine(DateTime.Now.ToString("HH:mm:ss - ") + s);
            }
            finally
            {
                sw.Close();
            }
        }

    }
}
